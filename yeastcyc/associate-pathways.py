#!/usr/bin/python

# Get a two-column file associating reactions to pathways, incl superpways

def recurPwy(rs, pr, debug=False):
    if debug:
        print("Called with rs=%s" % rs)
    if isinstance(rs, basestring):
        if debug: print("This seems to be a string, not a set")
        if not rs in pr.keys():
            if debug: print("%s doesn't seem to be a pathway" % rs)
            return(set(rs))
        else:
            if debug: print("%s seems to be a pathway" % rs)
            return(recurPwy(pr[rs], pr))
    if isinstance(rs, set):
        if debug: print("This seems to be a set, not a string")
        rset = set()
        for r in rs:
            if not (r in pr.keys()):
                if debug: print("%s seems to be a reaction, not a pathway" % r)
                rset.add(r)
            else:
                if debug: print("%s seems to be a pathway, not a reaction" % r)
                rset |= (recurPwy(pr[r], pr))
        return(rset)

from ypFlat import *
pwayToRxns = dict()
pwayP = ypDocument.parseFile("pathways.dat")
for record in pwayP:
    p = record['UID']
    rxns = [e[1] for e in record['attr'] if e[0] == "REACTION-LIST"]
    subp = [e[1] for e in record['attr'] if e[0] == "SUB-PATHWAYS"]
    pwayToRxns[p] = set(rxns).union(set(subp))
for p in pwayToRxns.keys():
    rxns = pwayToRxns[p]
    rxnset = recurPwy(rxns, pwayToRxns)
    for r in rxnset:
        print("%s\t%s" % (p, r))
