#!/usr/bin/python

# Parse enzrxns.dat and genes.dat to match rxn ids to gene products.

import sys
import os
import click
import pandas
from ypFlat import *


def warn(warning):
    sys.stderr.write(warning + "\n")

def MapProtAndCplx(pid, pidMap, pidIsCplx):
    mapped = set()
    if not pid in pidIsCplx.keys():
        raise("pid not in pidIsCplx.keys()")
    if not pidIsCplx[pid]:
        return(set([pidMap[pid]]))
    else:
        components = pidMap[pid]
        for c in components:
            mapped |= MapProtAndCplx(c, pidMap, pidIsCplx)
    return(mapped)

@click.command()
@click.option('--yppath',default='.',help='path to yeastcyc data')
@click.option('--sgdpath',default='.',help='path to SGD feature file')
@click.option('--sgdfeat',default='SGD_features.tab',help='name of SGD feature file')
def analyze(yppath, sgdpath, sgdfeat):

    protP = ypDocument.parseFile(os.path.join(yppath, "proteins.dat"))
    enzP = ypDocument.parseFile(os.path.join(yppath, "enzrxns.dat"))
    geneP = ypDocument.parseFile(os.path.join(yppath, "genes.dat"))

    reactionToEnzyme = dict()
    enzymeToProtID = dict()
    protIDToGeneOrComponents = dict()
    isProtIDComplex = dict()
    reactionToGeneID = dict()
    geneIDToGene = dict()

    for record in enzP:
        uid = record['UID']
        rxns = ypGet(record, 'REACTION')
        if len(rxns) > 0:
            for r in set(rxns):
                if not r in reactionToEnzyme.keys():
                    reactionToEnzyme[r] = set()
                reactionToEnzyme[r].add(uid)

    for record in protP:
        uid = record['UID']
        types = set(ypGet(record, 'TYPES'))
        if 'Protein-Complexes' in types:
            components = set(ypGet(record, 'COMPONENTS'))
            protIDToGeneOrComponents[uid] = components
            isProtIDComplex[uid] = True
        else:
            genes = ypGet(record, 'GENE')
            if len(genes) > 1:
                print(genes)
                raise Exception("Can't have multiple genes per protein")
            if len(genes) == 0:
                # nothing to do here
                continue
            gene = genes[0]
            protIDToGeneOrComponents[uid] = gene
            isProtIDComplex[uid] = False
        catalyzes = ypGet(record, 'CATALYZES')
        if len(catalyzes) > 0:
            for enzrxn in catalyzes:
                enzymeToProtID[enzrxn] = uid

    for record in geneP:
        uid = record['UID']
        cn = ypGet(record, 'COMMON-NAME')
        if len(cn) == 0:
            geneIDToGene[uid] = uid
        elif len(cn) == 1:
            if cn[0] == '':
                geneIDToGene[uid] = uid
            else:
                geneIDToGene[uid] = cn[0]
        else:
            warn("gene %s had multiple common names, picking one" % uid)
            geneIDToGene[uid] = cn[0]

    for rxn in reactionToEnzyme.keys():
        enzymes = reactionToEnzyme[rxn]
        if not rxn in reactionToGeneID.keys():
            reactionToGeneID[rxn] = set()
        for e in enzymes:
            if e in enzymeToProtID.keys():
                p = enzymeToProtID[e]
                g = MapProtAndCplx(p,
                                   protIDToGeneOrComponents,
                                   isProtIDComplex)
                reactionToGeneID[rxn] |= g
            else:
                warn("Missing enzyme %s" % e)

    sgd = pandas.read_table(os.path.join(sgdpath, sgdfeat),
                            header=None,
                            na_filter=False)
    commonToORF = dict()
    for n in range(len(sgd.index)):
        if not (sgd[3][n] == ''):
            if not (sgd[4][n] == ''):
                commonToORF[sgd[4][n]] = sgd[3][n]
            else:
                commonToORF[sgd[3][n]] = sgd[3][n]

    for r in reactionToGeneID.keys():
        gids = list(reactionToGeneID[r])
        for gid in gids:
            if gid in geneIDToGene:
                cn = geneIDToGene[gid]
            else:
                cn = gid
            if cn in commonToORF.keys():
                print("%s\t%s" % (commonToORF[cn], r))
            else:
                print("%s\t%s" % (cn, r))


#    enzToProt = dict()
#    cplxToComponents = dict()
#    pidToGene = dict()
#    protP = ypDocument.parseFile(os.path.join(yppath, "proteins.dat"))
#    # print("%d proteins found: " % len(protP))
#    for record in protP:
#        uid = record['UID']
#        catalyzes = [e[1] for e in record['attr'] if (e[0] == 'CATALYZES')]
#        types = [e[1] for e in record['attr'] if (e[0] == "TYPES")]
#        if "Protein-Complexes" in types:
#            components = [e[1] for e in record['attr'] if (e[0] == 'COMPONENTS')]
#            for c in components:
#                cplxToComponents[uid] = c
#            continue
#        genes = [e[1] for e in record['attr'] if (e[0] == 'GENE')]
#        if len(genes) > 1:
#            raise Exception("Each protein should be assigned to at most 1 gene")
#        if len(genes) == 0:
#            gene = record['UID']
#        else:
#            gene = genes[0]
#        for c in catalyzes:
#            if not c in enzToProt.keys():
#                enzToProt[c] = set()
#            enzToProt[c].add(gene)
#            if gene in pidToGene.keys():
#                raise("not supposed to be multiple genes associated with one PID")
#            pidToGene[uid] = gene
#
#    # This dict matches GENEIDs (UNIQUE-ID) to COMMON-NAME
#    geneidToGene = dict()
#    gEnzP = ypDocument.parseFile(os.path.join(yppath, "genes.dat"))
#    # print("%d gene entries found: " % len(gEnzP))
#    for record in gEnzP:
#        geneid = record['UID']
#        names = [e[1] for e in record['attr'] if (e[0] == 'COMMON-NAME')]
#        if len(names) > 1:
#            raise Exception("Each gene should only have one common name")
#        if len(names) == 0:
#            name = geneid
#        else:
#            name = names[0]
#        geneidToGene[geneid] = name
#
#    reactionToEnzyme = dict()
#    # This dict matches REACTION to UNIQUE-ID (in this case, e.g., ENZRXN3O-1003)
#    rxEnzP = ypDocument.parseFile(os.path.join(yppath, "enzrxns.dat"))
#    # print("%d enzrxn entries found: " % len(rxEnzP))
#    for record in rxEnzP:
#        rxns = [e[1] for e in record['attr'] if (e[0] == 'REACTION')]
#        for r in rxns:
#            if r not in reactionToEnzyme.keys():
#                reactionToEnzyme[r] = set()
#            reactionToEnzyme[r].add(record['UID'])
#
#    reactionToGene = dict()
#    for r in reactionToEnzyme.keys():
#        enzymes = list(reactionToEnzyme[r])
#        # print(enzymes)
#        geneids = set()
#        for e in enzymes:
#            if e in enzToProt.keys():
#                ps = enzToProt[e]
#                for p in ps:
#                    geneids.add(p)
#            elif e in cplxToComponents.keys():
#
#        geneids = list(geneids)
#        # print(geneids)
#        for g in geneids:
#            if g in geneidToGene.keys():
#                if not r in reactionToGene.keys():
#                    reactionToGene[r] = set()
#                reactionToGene[r].add(geneidToGene[g])
#            else:
#                # This only happens with complexes, which we're not considering
#                print("Warning: some genes not described (%s)" % g)
#                reactionToGene[r] = g
#
#    #print(reactionToEnzyme["PEPDEPHOS-RXN"])
#    #for r in reactionToEnzyme["PEPDEPHOS-RXN"]:
#    #    print(enzToProt[r])
#    #    for x in enzToProt[r]:
#    #        print(geneidToGene[x])
#    #sys.exit()
#

if __name__ == '__main__':
    analyze()

if False:
    testRecord = """
    UNIQUE-ID - ENZRXN3O-0
    TYPES - Enzymatic-Reactions
    COMMON-NAME - GDP-mannose:GlcNAc2-PP-dolichol beta-mannosyltransferase
    BASIS-FOR-ASSIGNMENT - :MANUAL
    ENZYME - YBR110W-MONOMER
    REACTION - 2.4.1.142-RXN
    //
    """
    testRecords = """
    #    REACTION-DIRECTION
    #    REGULATED-BY
    #    REQUIRED-PROTEIN-COMPLEX
    #    SYNONYMS
    #    TEMPERATURE-OPT
    #
    UNIQUE-ID - ENZRXN3O-0
    TYPES - Enzymatic-Reactions
    COMMON-NAME - GDP-mannose:GlcNAc2-PP-dolichol beta-mannosyltransferase
    BASIS-FOR-ASSIGNMENT - :MANUAL
    ENZYME - YBR110W-MONOMER
    REACTION - 2.4.1.142-RXN
    //
    UNIQUE-ID - ENZRXN3O-1
    TYPES - Enzymatic-Reactions
    BASIS-FOR-ASSIGNMENT - :MANUAL
    ENZYME - YBR084W-MONOMER
    REACTION - METHENYLTHFCYCLOHYDRO-RXN
    //
    UNIQUE-ID - ENZRXN3O-10
    TYPES - Enzymatic-Reactions
    BASIS-FOR-ASSIGNMENT - :MANUAL
    ENZYME - YGR060W-MONOMER
    REACTION - RXN3O-154
    //
    """
    print(testRecords)
    print(ypEntry.parseString("COMMON-NAME - nil"))
    print(ypRecord.parseString(testRecord))
    print(ypDocument.parseString(testRecords))
