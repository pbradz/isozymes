# Script to parse MetaCyc data

metacyc.path <- "data/yeastcyc/"

metacyc.dat.parse <- function(datf, name = TRUE) {
  all.lines <- scan(datf, "character", sep = "\n", comment.char = "#")
  start.breaks <- c(0, which(all.lines == '//')) + 1
  end.breaks <- (start.breaks - 1)[-1] - 1
  parsed <- lapply(1:length(end.breaks), function(i) {
    lines <- (all.lines[start.breaks[i]:end.breaks[i]])
    dimnames(lines) <- NULL
    tmp <- cbind(i, Reduce(rbind, strsplit(lines, " - ")))
    colnames(tmp) <- c("number","key","value")
    tmp
  })
  if (name) {
    names(parsed) <- sapply(parsed, function(x) x[which(x[, "key"] == "UNIQUE-ID"), "value"])
  }
  parsed
}

prot <- metacyc.dat.parse(file.path(metacyc.path, "proteins.dat"))
erx <- metacyc.dat.parse(file.path(metacyc.path, "enzrxns.dat"))
rxn <- metacyc.dat.parse(file.path(metacyc.path, "reactions.dat"))
pw <- metacyc.dat.parse(file.path(metacyc.path, "pathways.dat"))
gn <- metacyc.dat.parse(file.path(metacyc.path, "genes.dat"))

non.super.pws <- pw[!sapply(pw, function(p) "Super-Pathways" %in% p[which(p[, "key"] == "TYPES"), "value"])]
parsed <- lapply(non.super.pws, function(p) {
  reactions <- p[which(p[, "key"] == "REACTION-LIST"), "value"]
  enzrxns <- lapply(reactions, function(r) { tmp <- rxn[which(names(rxn) == r)]; sapply(tmp, function(tm) tm[which(tm[,"key"] == "ENZYMATIC-REACTION"), "value"]) })
  proteins <- lapply(unlist(enzrxns), function(e) { tmp <- erx[which(names(erx) == e)]; sapply(tmp, function(tm) tm[which(tm[,"key"] == "ENZYME"), "value"]) })
  pr <- unlist(proteins) 
  # take care of complexes
  pr.components <- lapply(unlist(proteins), function(p) {
    this.pro <- prot[which(names(prot) == p)]
    sapply(this.pro, function(this.p) {
      components <- NULL
      if ("COMPONENTS" %in% this.p[, "key"]) {
        components <- this.p[which(this.p[, "key"] == "COMPONENTS"), "value"]
        while (any(names(components) %in% names(prot))) {
          components <- unlist(sapply(components, function(com) {
              tp <- prot[which(names(prot) == com)] 
              if ("COMPONENTS" %in% tp[, "key"]) {
                return(tp[which(tp[, "key"] == "COMPONENTS"), "value"])
              } else {
                return(tp[which(tp[, "key"] == "GENE"), "value"])
              }
          }))
        }
      }
      components
    })
  })
  genes <- lapply(unlist(c(proteins, pr.components)), function(p) { tmp <- prot[which(names(prot) == p)]; sapply(tmp, function(tm) tm[which(tm[,"key"] == "GENE"), "value"]) })
  accessions <- lapply(unlist(genes), function(g) { tmp <- gn[which(names(gn) == g)]; sapply(tmp, function(tm) {
      if ("ACCESSION-1" %in% tm[,"key"]) {
        tm[which(tm[,"key"] == "ACCESSION-1"), "value"]
      } else {
        tm[which(tm[,"key"] == "COMMON-NAME"), "value"]
      }
    })
  })
  unique(c(as.character(unlist(accessions))))
})

saveRDS(file = "pathways.rds", parsed)
pw.3 <- parsed[which(sapply(parsed, length) >= 3)]
saveRDS(file = "pathways-3plus.rds", pw.3)
all.pairs <- t(Reduce(cbind, lapply(pw.3, function(x) combn(x, 2))))
write.csv(file="all-pairs.csv", all.pairs)
sampled.pairs <- all.pairs[sample(1:nrow(all.pairs), 500, replace = FALSE),]
write.csv(file="sampled-pairs.csv", sampled.pairs)
