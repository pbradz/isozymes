roster <- read.table("E-MTAB-112.sdrf.txt", sep = "\t", stringsAsFactors = FALSE, header = TRUE)
array.data <- lapply(roster$Array.Data.File, function(x) read.table(x, header = TRUE, stringsAsFactors = FALSE, sep = '\t'))

# default is Cy5/Cy3 ("red"/"green", really "purple", "red"); actually don't
# invert if label = "Cy3" because the dye swaps seem to have been already done

process.array <- function(A, invert = FALSE) {
  intensities <- log((A$Spot.Rmean - A$Spot.bgRmean) / (A$Spot.Gmean - A$Spot.bgGmean))
  if (invert) intensities <- -intensities
  unavg <- data.frame(id = A$Reporter.identifier, lr = intensities, bad = A$Spot.badspot)
  unavg$lr[unavg$bad != 0] <- NA
  aggregate(list(lr = unavg$lr), by = list(id = unavg$id), FUN=median, na.rm = TRUE) 
}

library(pbapply)
processed.data <- pbapply(roster, 1, function(R) {
  A <- read.table(R["Array.Data.File"], header = TRUE, stringsAsFactors = FALSE, sep = '\t')
  inv <- (R["Label"] == "Cy3")
  process.array(A, FALSE)
})

names(processed.data) <- NULL
for (x in names(processed.data)) colnames(processed.data[[x]][2]) <- x
rsamples <- unique(roster$Hybridization.Name)
average.dyeswaps <- pblapply(rsamples, function(r) {
  avgd <- Reduce(function(a,b) merge(a,b,by="id",all=TRUE), processed.data[which(roster$Hybridization.Name== r)])
  av2 <- data.frame(id = avgd[,1], val = apply(avgd[,-1], 1, mean))
  colnames(av2)[2] <- r
  av2
})
names(average.dyeswaps) <- rsamples
all.dsamples <- gsub("_ds", "", average.dyeswaps %>% names)
collapsed <- pblapply(unique(all.dsamples), function(d) {
  avgd <- Reduce(function(a,b) merge(a,b,by="id",all=TRUE), average.dyeswaps[which(all.dsamples == d)])
  if (dim(avgd)[2] == 2) return(avgd)
  av2 <- data.frame(id = avgd[,1], val = apply(avgd[,-1], 1, mean))
  colnames(av2)[2] <- d
  av2
})
names(collapsed) <- lapply(collapsed, function(x) colnames(x)[2])
mtx <- Reduce(function(a,b) merge(a,b,by="id", all=TRUE), collapsed)
mtx.1 <- mtx[,-1]
rownames(mtx.1) <- gsub("_01$", "", gsub("ebi.ac.uk:MIAMExpress:Reporter:A-MEXP-1185.", "", mtx[,1])) %>% gsub("01$", "", .) %>% gsub("Y(......)(.)","Y\\1-\\2",.)
library(impute)
mtx.i <- impute.knn(data.matrix(mtx.1))
mtx.id <- mtx.i$data
mtx.idc <- mtx.id[, order(colnames(mtx.id))]
mtx.idc.noself <- mtx.idc[, grep("self", colnames(mtx.idc), invert = TRUE)]
write.csv(file="carreto.csv", mtx.idc.noself)
