#!/bin/bash

for x in ../*.aln; do
	raxmlHPC-PTHREADS-AVX -f a -p 2018 -x 2018 -T 8 -m PROTGAMMAAUTO -s $x -n `basename $x .aln`.tree -# 100
done	
